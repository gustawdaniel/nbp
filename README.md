Run

```
[...document.querySelectorAll('.normal_2 a')]
    .map(a => `wget ${a.href}`)
    .filter(link => /archiwum_tab/.test(link))
    .join(' && ')
```

on page

https://www.nbp.pl/home.aspx?f=/kursy/arch_a.html

Result paste into `raw` directory and execute

Then convert `xls` to `csv` by

```
for i in *.xls; do  libreoffice --headless --convert-to csv "$i" ; done                                                     
```

At the end generate result using

```
npm i
ts-node app.ts
```