import ApexCharts from 'apexcharts'
import {CHF} from './out/chf.json'

interface StockRecord {
    x: Date,
    y: [number, number, number, number]
}

const splitDateIntoEqualIntervals = (startDate: Date, endDate: Date, numberOfIntervals: number): { start: Date, end: Date, avg: Date }[] => {
    const intervalLength = (endDate.getTime() - startDate.getTime()) / numberOfIntervals
    return [...(new Array(numberOfIntervals))]
        .map((e, i) => {
            return {
                start: new Date(startDate.getTime() + i * intervalLength),
                avg: new Date(startDate.getTime() + (i + 0.5) * intervalLength),
                end: new Date(startDate.getTime() + (i + 1) * intervalLength)
            }
        })
}

const mapToStockData = (values: { [key: string]: number }, parts: number):StockRecord[] => {
    const entries = Object.entries(values)
    const start = new Date(entries[0][0])
    const end = new Date(entries[entries.length - 1][0])
    const intervals = splitDateIntoEqualIntervals(start, end, parts)

    const stockData: StockRecord[] = []

    while (intervals.length) {
        const int = intervals.shift()
        if (!int) break
        let currDate = int.start
        stockData.push({
            x: int.avg,
            y: [NaN, NaN, NaN, NaN]
        })

        const currStock = stockData[stockData.length - 1]
        let stat = {
            min: Infinity,
            max: -Infinity
        }

        while (currDate < int.end) {
            const [dateString, value] = entries.shift() || []
            if (!dateString || typeof value !== 'number') break
            currDate = new Date(dateString)
            if (isNaN(currStock.y[0])) currStock.y[0] = value
            currStock.y[3] = value
            stat.min = Math.min(stat.min, value)
            stat.max = Math.max(stat.max, value)
        }
        currStock.y[1] = stat.max
        currStock.y[2] = stat.min
    }

    return stockData
}

const generateOptions = (data: StockRecord[]) => ({
    series: [{
        data
    }],
    chart: {
        type: 'candlestick',
        height: window.innerHeight - 50,
        zoom: {
            autoScaleYaxis: true
        }
    },
    title: {
        text: 'CHF to PLN',
        align: 'left'
    },
    xaxis: {
        type: 'datetime'
    },
    yaxis: {
        tooltip: {
            enabled: true
        }
    }
})

const chart = new ApexCharts(document.querySelector('#chart'), generateOptions(mapToStockData(CHF, 500)))
chart.render().then(console.log).catch(console.error)
