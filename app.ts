import fs from 'fs'
import chai from 'chai'

const dict: { [key: string]: string } = {
    'Szwajcaria': 'CHF'
}

interface YearData {
    [key: string]: {
        col: number,
        div: number,
        values: { [key: string]: number }[]
    }
}

interface OutData {
    [key: string]: {
        [key: string]: number
    }
}

const mergeYears = (payload: YearData[]): OutData => {
    return payload.reduce((p: OutData, n: YearData) => {
        Object.keys(n).forEach(key => {
            if (p.hasOwnProperty(key)) {
                p[key] = {...p[key], ...n[key].values.reduce((p, n) => ({...p, ...n}))}
            } else {
                p[key] = n[key].values.reduce((p, n) => ({...p, ...n}))
            }
        })
        return p
    }, {})
}


const getDateFromArr = (arr: string[]) => {
    return getDate(arr[0]) || getDate(arr[1])
}

const getDate = (input: string) => {
    if (/^\d{2}\.\d{2}\.\d{4}/.test(input)) {
        return input.split('.').reverse().join('-')
    }
    if (/^\d{2}\/\d{2}\/\d{4}/.test(input)) {
        const [m, d, y] = input.split('/')
        return [y, m, d].join('-')
    }
    return false
}


const decomposeBaseSettingsFromNames = (localArr: string[]) => localArr.reduce((p: YearData, n: string, i: number): YearData => {
    if (Object.keys(dict).includes(n)) {
        p[dict[n]] = {col: i, div: NaN, values: []}
    }
    return p
}, {})

const decomposeBaseSettingsFromCodes = (localArr: string[]) => localArr.reduce((p: YearData, n: string, i: number): YearData => {
    const [, div, curr] = n.match(/^(\d*)(\w+)$/) || []
    if (curr && Object.values(dict).includes(curr)) {
        p[curr] = {col: i, div: parseInt(div), values: []}
    }
    return p
}, {})

const extendSettingsByDivCoefficient = (arr: string[][], settings: YearData) => {
    const subHead = arr.shift()
    if (!subHead) throw Error('File do not have sub-header line.')
    Object.keys(settings).forEach(key => {
        settings[key].div = parseInt(subHead[settings[key].col])
    })
}

const denominationFactor = (date:string): number => {
    return Number.parseInt(date.substr(0,4)) <= 1994 ? 1e4 : 1;
}

const recognizeSettingsFromHead = (arr: string[][]): YearData => {
    const head = arr.shift()
    if (!head) throw Error('File do not have header line.')
    let settings: YearData = decomposeBaseSettingsFromNames(head)
    if (Object.keys(settings).length) {
        extendSettingsByDivCoefficient(arr, settings);
    } else {
        settings = decomposeBaseSettingsFromCodes(head);
        while (Object.keys(settings).some(key => Number.isNaN(settings[key].div))) {
            extendSettingsByDivCoefficient(arr, settings);
        }
    }

    return settings;
}

const FILES_FILTER = (e: string, i: number) => i <= Infinity
const ROWS_FILTER = (e: string, i: number) => i <= Infinity

const DROP_SPACES = (l: string): string => l.replace(/\s+/g, '')
const DROP_JUNK_LINES = (l: string): string => l.replace(/(Nr)|(data)|(WALUTA\/CURRENCY)|(\.tab)/ig, '')
const DROP_EMPTY_LINES = (e: string) => !/^,*$/.test(e)

const testYearData = (r: YearData): void => {
    chai.expect(r).to.haveOwnProperty('CHF');
    chai.expect(r.CHF).to.haveOwnProperty('col');
    chai.expect(r.CHF).to.haveOwnProperty('div');
    chai.expect(r.CHF).to.haveOwnProperty('values');
    chai.expect(r.CHF.col).to.be.a('number');
    chai.expect(r.CHF.div).to.be.a('number');
    chai.expect(r.CHF.values).to.be.a('array');
    chai.expect(r.CHF.values.length).to.be.greaterThan(0);
    r.CHF.values.forEach(v => {
        chai.expect(Object.keys(v)[0]).to.be.a('string');
        chai.expect(/\d{4}-\d{2}-\d{2}/.test(Object.keys(v)[0])).to.be.true;
        chai.expect(Object.values(v)[0]).to.be.a('number');
        chai.expect(Object.values(v)[0]).to.be.greaterThan(0);
    })
};

const main = () => {
    const rawDir = process.cwd() + `/raw`

    return mergeYears(fs.readdirSync(rawDir).filter(f => f.endsWith('csv'))
        .filter(FILES_FILTER)
        .map((name, i) => {
            const arr = fs
                .readFileSync(`${rawDir}/${name}`)
                .toString()
                .split(`\n`)
                .map(DROP_SPACES)
                .map(DROP_JUNK_LINES)
                .filter(DROP_EMPTY_LINES)
                .filter(ROWS_FILTER)
                .map(l => l.split(','));

            let settings: YearData = recognizeSettingsFromHead(arr);

            arr.forEach(localArr => {
                const date = getDateFromArr(localArr)

                const newSettings = decomposeBaseSettingsFromCodes(localArr)
                if (Object.keys(newSettings).length) {
                    Object.keys(settings).forEach(key => {
                        settings[key].div = newSettings[key].div
                    })
                }

                if (typeof date === 'string') {
                    Object.keys(settings).forEach(key => {
                        settings[key].values.push({[date]: parseFloat(localArr[settings[key].col]) / settings[key].div / denominationFactor(date)})
                    })
                }
            })

            testYearData(settings);

            return settings;

        }))
}

!fs.existsSync(process.cwd() + '/out') && fs.mkdirSync(process.cwd() + '/out', {recursive: true})
fs.writeFileSync(process.cwd() + '/out/chf.json', JSON.stringify(main()))